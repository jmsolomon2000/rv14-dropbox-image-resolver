package main

import (
  "github.com/dropbox/dropbox-sdk-go-unofficial/dropbox/files"
  "github.com/dropbox/dropbox-sdk-go-unofficial/dropbox"
  "fmt"
  "io/ioutil"
  "os"
  "flag"
  "path"
  "regexp"
  "log"
  )

func main() {

  token := flag.String("token", "", "Dropbox Token")
  mdFile := flag.String("mdFile", "", "Markdown File")
  imgDir := flag.String("staticDir", "", "Static file directory")
  debug := flag.Bool("debug", false, "enable debug logging")
  flag.Parse()

  if *debug == false {
    log.SetOutput(ioutil.Discard)
  }

  dbConfig := dropbox.Config{Token: *token, Verbose: true}

  figureRegEx, _ := regexp.Compile("{{\\s?<\\s?figure.+?src=\"(.+?)\"")
  mdFileContent, _ := ioutil.ReadFile(*mdFile)

  for _, match := range figureRegEx.FindAllStringSubmatch(string(mdFileContent), -1) {
    locationToStoreImage := path.Join(*imgDir, match[1])
    //mkdir if it doesn't exist
    os.MkdirAll(path.Dir(locationToStoreImage), os.ModePerm)
    downloadFile(path.Base(locationToStoreImage) , locationToStoreImage, dbConfig)
  }
}

func downloadFile(imgNameToDl string, locationToStore string, dbConfig dropbox.Config){
  dlArg := files.NewDownloadArg(fmt.Sprintf("/Camera Uploads/%s", imgNameToDl))
  dbx := files.New(dbConfig)
  _, rClose, _ := dbx.Download(dlArg)
  fileContent, _ := ioutil.ReadAll(rClose)
  defer rClose.Close()

  err := ioutil.WriteFile(locationToStore, fileContent, os.ModePerm)

  if err != nil {
    log.Fatal(err)
  }
  fmt.Println(locationToStore)
}
